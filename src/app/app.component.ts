import { Component } from '@angular/core';
import { SideNavElement } from './interfaces/SideNavElement';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularsample';
  navbarElements: SideNavElement[] = [
    {
      text: 'Home',
      url: ''
    },
    {
      text: 'Dashboard',
      url: 'dashboard'
    }
  ];
}
