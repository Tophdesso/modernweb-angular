import { Component, OnInit } from '@angular/core';
import { APIFetcher } from '../http/APIFetcher';
import { IModernWebEntity, IUser, IRole } from '../interfaces/ODataResult';
import { MatTableDataSource } from '@angular/material';
import { FormBuilder, Form, FormControl, Validators } from '@angular/forms';
import { IMatSelectItem } from '../interfaces/IMatSelectItem';
import { IMatColumnDefinition } from '../interfaces/MatFuncTableInterfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /**
   * The user form to add and edit users
   */
  userForm;

  /**
   * The roles that can be assigned to a user
   */

  roles: IRole[] = [];

  /**
   * The table's datasource.
   */
  users: MatTableDataSource<IUser>;

  /**
   * Provides information on which columns should be displayed and which property is attached to them
   */
  public ColumnDefinitions: IMatColumnDefinition[] = [
    {
      Name: 'Id',
      PropertyAccessor: 'Id',
      Visible: false
    },
    {
      Name: 'FirstName',
      PropertyAccessor: 'FirstName',
    },
    {
      Name: 'LastName',
      PropertyAccessor: 'LastName'
    },
    {
      Name: 'Role',
      PropertyAccessor: 'Role.Name',
    }];


  constructor(private fetcher: APIFetcher, private formBuilder: FormBuilder) { }

  async ngOnInit() {
    this.users = new MatTableDataSource(await this.FetchUsers());
  }

  /**
   * Callback for adding an entity to the list.
   */
  public async Add() {
    this.roles = await this.FetchRoles();
    this.BuildForm();
  }

  /**
   * Callback for editing an entity.
   */
  public async Edit(data: IUser) {
    let currentUserData = (await this.fetcher.Get<IUser>("user(" + data.Id + ")?$expand=Role"))[0];
    this.roles = await this.FetchRoles();
    this.BuildForm(currentUserData);
  }

  /**
   * Callback for deleting an entity.
   */
  public async Delete(data: IUser) {
    await this.fetcher.Delete('user(' + data.Id + ')');
    this.users = new MatTableDataSource(await this.FetchUsers());
  }

  /**
   * Fetches the users and forces the loading spinner to appear.
   */
  private async FetchUsers(): Promise<IUser[]> {
    this.users = null;
    return await this.fetcher.Get<IUser>('user?$expand=Role($select=Id,Name)');
  }


  /**
   * Fetches the roles for user adding / editing.
   */
  private async FetchRoles(): Promise<IRole[]> {
    this.roles = [];
    return await this.fetcher.Get<IRole>('role?$select=Id,Name');
  }

  /**
   * Used to initialize the form.
   * @param user (Optional) data to populate the form.
   */
  private BuildForm(user?: IUser) {
    this.userForm = this.formBuilder.group({
      FirstName: new FormControl({ value: user ? user.FirstName : '', disabled: false }, Validators.required),
      LastName: new FormControl({ value: user ? user.LastName : '', disabled: false }, Validators.required),
      RoleId: new FormControl({ value: user && user.Role ? user.Role.Id : '', disabled: false }, Validators.required),
      Id: new FormControl({ value: user ? user.Id : '', disabled: false }),
    });
  }

  /**
   * 
   * @param form Triggered when a user is saved.
   */
  public async OnSubmit(value: any, valid: boolean) {
    if (valid) {
      let user: IUser = {
        FirstName: value.FirstName,
        LastName: value.LastName,
        Role: {
          Id: value.RoleId
        }
      };

      let prom: Promise<any>;

      if (value.Id) {
        user.Id = value.Id;
        prom = this.fetcher.Put('user', user);
      }
      else prom = this.fetcher.Post<IUser>('user', user);

      this.userForm = null;

      await prom;

      this.users = new MatTableDataSource(await this.FetchUsers());
    }
  }

  /**
   * Hides the edit dialog.
   */
  public Cancel() {
    this.userForm = null;
  }
}
