import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IMatTableFuncDataSource, IMatColumnDefinition } from '../interfaces/MatFuncTableInterfaces';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { IUser } from '../interfaces/ODataResult';

@Component({
  selector: 'app-multi-func-table',
  templateUrl: './multi-func-table.component.html',
  styleUrls: ['./multi-func-table.component.scss']
})
export class MultiFuncTableComponent<T extends IMatTableFuncDataSource> implements OnInit {

  /*********** Input members ***********/
  @Input()
  DataSource: T[] | MatTableDataSource<T>;

  @Input()
  ColumnDefinitions: IMatColumnDefinition[];

  @Input()
  SelectedBackgroundColor = '#3f51b5';

  @Input()
  SelectedFontColor = 'white';

  @Input()
  Title = '';

  /*********** *************** ***********/
  /*********** Output members ***********/

  @Output()
  add: EventEmitter<any> = new EventEmitter();

  @Output()
  edit: EventEmitter<any> = new EventEmitter();

  @Output()
  delete: EventEmitter<any> = new EventEmitter();

  @Output()
  rowClicked: EventEmitter<any> = new EventEmitter();

  /*********** *************** ***********/
  /*********** Viewchild members ***********/

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  /*********** *************** ***********/
  /*********** Component members ***********/

  MatTableDataSource: MatTableDataSource<T>;

  SelectedRow: T;

  VisibleColumns: string[] = [];

  /*********** *************** ***********/
  constructor() { }

  ngOnInit() {
    if (this.DataSource instanceof MatTableDataSource) {
      this.MatTableDataSource = this.DataSource;
    }
    else {
      this.MatTableDataSource = new MatTableDataSource(<T[]>this.DataSource);
    }

    this.MatTableDataSource.paginator = this.paginator;

    this.ColumnDefinitions.forEach(def => {
      if(def.Visible === undefined) def.Visible = true;
      if (def.Visible) this.VisibleColumns.push(def.Name);
    });
  }

  public GetPropertyValue(element: T, property: string) {
    let layers: string[] = property.split('.');
    let val;
    val = element[layers[0]];
    for (let i = 1; i < layers.length; i++) {
      val = val[layers[i]];
    }

    return val;
  }

  public RowClicked(row: T) {
    this.SelectedRow = row;
    this.rowClicked.emit(this.SelectedRow);
  }
}
