import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiFuncTableComponent } from './multi-func-table.component';

describe('MultiFuncTableComponent', () => {
  let component: MultiFuncTableComponent;
  let fixture: ComponentFixture<MultiFuncTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiFuncTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiFuncTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
