import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { IModernWebEntity, IODataResult } from '../interfaces/ODataResult';

@Injectable({
    providedIn: 'root',
})
export class APIFetcher {
    
    constructor(private httpClient: HttpClient) { }

    public async Get<T extends IModernWebEntity>(url: string): Promise<T[]> {
        let rc: IODataResult<T>;
        await this.httpClient.get(environment.apiBaseURL + '/' + url)
            .toPromise()
            .then((suc: IODataResult<T>) => {
                rc = suc
            });
        return rc.value;

    }
    public async Post<T>(url: string, data: T): Promise<any> {
        let header = new HttpHeaders().set('Content-type', 'application/json');
        let rc;
        await this.httpClient.post(environment.apiBaseURL + '/' + url, data, { headers: header})
            .toPromise()
            .then(suc => {
                rc = suc
            });
        return rc;
    }
    public async Put<T>(url: string, data: T): Promise<any> {
        let rc;
        await this.httpClient.put(environment.apiBaseURL + '/' + url, data)
            .toPromise()
            .then(suc => {
                rc = suc
            });
        return rc;
    }
    public async Delete(url: string): Promise<Object> {
        return await this.httpClient.delete(environment.apiBaseURL + '/' + url)
            .toPromise();
    }
}