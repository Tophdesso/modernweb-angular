export interface IMatTableFuncDataSource {

}

export interface IMatColumnDefinition {
    Name: string;
    PropertyAccessor: string;
    Visible?: boolean;
}