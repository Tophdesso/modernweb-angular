export interface IMatSelectItem {
    value: string | number;
    text: string;
}