export interface SideNavElement{
    text:string;
    url:string;
}