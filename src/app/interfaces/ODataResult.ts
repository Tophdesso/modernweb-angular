export interface IModernWebEntity {
    Id?: string;
}
export interface IODataResult<T extends IModernWebEntity> {
    "@odata.context": string;
    value: T[];
}

export interface IUser extends IModernWebEntity {
    FirstName?: string;
    LastName?: string;
    Role?: IRole;
}

export interface IRole extends IModernWebEntity {
    Name?: string;
    Users?: IUser[];
}